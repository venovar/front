import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from '../../model/user';
import 'rxjs/Observable';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  public store(user: User, success: Function, error?: Function) {
    let url = environment.service + '/api/guest/user/store';
    this.http
      .post<any>(url, user)
      .subscribe(
      data => {
        success(data.message);
      },
      data => {
        if (error) {
          let messages = ''
          for (var prop in data.error.errors) {
            let error = data.error.errors[prop];
            messages += error + '\r\n';
          }
          error(messages);
        }
      });
  }
}
