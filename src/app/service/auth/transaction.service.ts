import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from '../../model/user';
import { map } from 'rxjs/operators';
import 'rxjs/Observable';

@Injectable()
export class TransactionService {

  constructor(private http: HttpClient) { }

  public users(page?: number, term?: string, success?: Function, error?: Function) {
    let url = environment.service + '/api/transaction/users';
    let params = new HttpParams();
    if (page)
      params = params.append("page", page.toString());
    if (term)
      params = params.append("term", term);
    this.http.get<any>(url, { params: params }).subscribe(data => {
      success(data.users);
    });
  }

  public index(page?: number, success?: Function, error?: Function) {
    let url = environment.service + '/api/transaction/index';
    let params = new HttpParams();
    if (page)
      params = params.append("page", page.toString());
    this.http.get<any>(url, { params: params }).subscribe(data => {
      success(data.transactions);
    });
  }

  public store(amount: number, users: User[], success: Function, error: Function) {
    let url = environment.service + '/api/transaction/store';
    this.http
      .post<any>(url, {
        amount: amount,
        to: users,
      } )
      .subscribe(
      data => {
        success(data.message);
      },
      data => {

        let messages = ''
        for (var prop in data.error.errors) {
          let error = data.error.errors[prop];
          messages += error + '\r\n';
        }
        error(messages);
      });
  }
}