import { AccountService } from './account.service';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Notification, Notifications } from '../../model/notification';
import Pusher from 'pusher-js';

@Injectable()
export class NotificationService {

  public indexBehavior: Subject<Notifications>;
  public notificationBehavior: Subject<Notifications>;

  private socket = new Pusher('10612111becb8f8ecff4', { cluster: 'us2' });
  private channel;

  constructor(private http: HttpClient, private accountService?: AccountService) {
    this.indexBehavior = new Subject<Notifications>();
    this.notificationBehavior = new Subject<Notifications>();
    this.channel = this.socket.subscribe('transaction-' + accountService.getUser().id);
    this.channel.bind('App\\Events\\NotificationEvent', this.transactionSocket.bind(this));
  }

  private transactionSocket(data) {
    this.notificationBehavior.next(data);
  }

  public index(page?: number) {
    let url = environment.service + '/api/notification/index';
    let params = new HttpParams();
    if (page)
      params = params.append("page", page.toString());
    return this.http.get<any>(url, { params: params }).subscribe(data => {
      this.indexBehavior.next(data.notifications);
    });
  }

  public read(data: Notification[], success?: Function) {
    let url = environment.service + '/api/notification/read';
    this.http.put<any>(url, { notification: data }).subscribe(data => {
      if (success)
        success();
    });
  }
}
