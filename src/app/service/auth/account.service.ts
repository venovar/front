import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../model/user';
import { environment } from '../../../environments/environment';

@Injectable()
export class AccountService {

  public urlShow: string = environment.service + '/api/account/show';
  private user: User;
  public userBehavior: Subject<User>;

  constructor(private http: HttpClient) {
    this.userBehavior = new Subject<User>();
    this.user = JSON.parse(localStorage.getItem('account'));
  }

  public getUser(fromServer?: boolean): User {
    if (fromServer) {
      this.http.get<any>(this.urlShow).subscribe(this.userFromService.bind(this));
    }
    return this.user;
  }

  private userFromService(data: any) {
    this.setUser(data.user);
    this.userBehavior.next(data.user);
  }

  public setUser(value: User) {
    localStorage.setItem('account', JSON.stringify(value));
    this.user = value;
  }
}
