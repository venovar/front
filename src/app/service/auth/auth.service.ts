import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Notification, Notifications } from './../../model/notification';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { User } from '../../model/user';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  public login(user: User, success: Function, error?: Function) {
    let url = environment.service + '/api/guest/auth/login';
    let data = {
      username: user.email,
      password: user.password,
      grant_type: environment.passport.grant_type,
      client_id: environment.passport.client_id,
      client_secret: environment.passport.client_secret,
    }
    this.http
      .post<any>(url, data)
      .subscribe(
      data => {
        localStorage.setItem('api_token', data.token_type + " " + data.access_token);
        success();
      },
      data => {
        error();
      });
  }

  public logout() {
    this.router.navigate(['login']);
    localStorage.removeItem('api_token');

  }

  public checkAuth(): boolean {
    return localStorage.getItem('api_token') != null && localStorage.getItem('api_token').trim().length > 0;
  }
}
