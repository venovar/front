export interface User {
    id: number;
    name: string;
    email: string;
    password: string;
    password_confirmation: string;
    amount: number;
    created_at: string;
    updated_at: string;
    checked: boolean;
}

export interface Users {
    next_page_url: string;
    total: number;
    data: User[];
}