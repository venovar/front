import { User } from "./user";

export interface Notification {
    user_id: number;
    title: string;
    details: string;
    readed: boolean;
    notifable_id: number;
    notifable_type: string;
    created_at: string;
    updated_at: string;
    notifable: any;
}

export interface Notifications {
    from: number;
    total_unread: number;
    next_page_url: string;
    total: number;
    data: Notification[];
}

export interface NotificationSocket {
    user: User;
    notification: Notification;
}