import { User } from './user';

export interface Transaction {
    from_id: number;
    to_id: number;
    amount: number;
    created_at: string;
    updated_at: string;
    from: User;
    to: User;
}

export interface Transactions {
    next_page_url: string;
    total: number;
    data: Transaction[];
}