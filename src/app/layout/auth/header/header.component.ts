import { AuthService } from './../../../service/auth/auth.service';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NotificationService } from '../../../service/auth/notification.service';
import { User } from '../../../model/user';
import { Notification, Notifications, NotificationSocket } from '../../../model/notification';
import { Router, NavigationEnd } from '@angular/router';
import { AccountService } from '../../../service/auth/account.service';

@Component({
  selector: 'auth-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class AuthHeaderComponent implements OnInit {

  public account: User;
  public notifications: Notifications;

  constructor(
    private authService: AuthService, //
    private accountService: AccountService, //
    private notificationService: NotificationService, //
    private router: Router
  ) {
    this.notificationService.indexBehavior.subscribe(this.indexBehavior.bind(this));
    this.notificationService.notificationBehavior.subscribe(this.notificationBehavior.bind(this));
  }

  public ngOnInit() {
    // When change the page will turn 0 the this.notifications.total_unread
    this.router.events
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          // When leaves notifications page.
          if (localStorage.getItem('previousPage') && localStorage.getItem('previousPage').indexOf('notification/index') >= 0) {
            this.notificationService.index();
          }
          localStorage.setItem('previousPage', event.url);
        }
      });
    this.notificationService.index();
    this.account = this.accountService.getUser();
  }

  private notificationBehavior(data: NotificationSocket) {
    if (data) {
      if (data.user) {
        this.account.amount = data.user.amount;
      }
      if (this.notifications == null) {
        this.notifications = <Notifications>{ data: [] };

      }
      if (this.notifications.data.length >= 10) {
        this.notifications.data.pop();
      }
      this.notifications.data = [data.notification].concat(this.notifications.data);
      this.notifications.total_unread++;
    }
  }

  private indexBehavior(value: Notifications) {
    this.notifications = value;
  }

  private indexSuccess(data: Notifications) {
    this.notifications = data;
  }

  public logout() {
    this.authService.logout();
  }
}
