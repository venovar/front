import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'guest-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class GuestHeaderComponent implements OnInit {

  constructor() { }

  public ngOnInit() { }

}
