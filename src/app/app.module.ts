import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { GuestHeaderComponent } from './layout/guest/header/header.component';
import { AuthHeaderComponent } from './layout/auth/header/header.component';
import { GuestModule } from './page/guest/guest.module';
import { AuthModule } from './page/auth/auth.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GuestComponent } from './page/guest/guest.component';
import { AuthComponent } from './page/auth/auth.component';
import { AuthInterceptor } from './page/auth/auth.interceptor';

const appRoutes: Routes = [
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    /* Layout */
    GuestHeaderComponent,
    AuthHeaderComponent,
    /* 404 */
    PageNotFoundComponent,
    /* Guest */
    GuestComponent,
    /* Auth */
    AuthComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot(),
    GuestModule,
    AuthModule,
    BrowserModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }