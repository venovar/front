import { AccountService } from './../../service/auth/account.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { environment } from '../../../environments/environment';

@Injectable()
export class AccountResolve implements Resolve<User> {

    constructor(private http: HttpClient, private accountService: AccountService) { }

    resolve(): Promise<any> {
        let promise = (resolve, reject) => {
            this.http.get<any>(this.accountService.urlShow).subscribe(
                data => {
                    this.accountService.setUser(data.user);
                    resolve();
                },
                data => {
                    reject('It was impossible to get account information. Please contact us!');
                }
            );
        }
        return new Promise(promise);
    }
}