import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NotificationService } from '../../../../service/auth/notification.service';
import { Notification, Notifications, NotificationSocket } from '../../../../model/notification';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
})
export class NotificationIndexComponent implements OnInit {

  private page: number;
  public notifications: Notifications;

  constructor(private http: HttpClient, private notificationService: NotificationService) {
    this.notificationService.indexBehavior.subscribe(this.indexBehavior.bind(this));
    this.notificationService.notificationBehavior.subscribe(this.notificationBehavior.bind(this));
  }

  public ngOnInit() {
    this.page = 1;
    this.notifications == null;
    this.index();
  }

  public index() {
    this.notificationService.index(this.page);
    this.page++;
  }

  private notificationBehavior(data: NotificationSocket) {
    if (data) {
      if (this.notifications == null) {
        this.notifications = <Notifications>{ data: [] };
      }
      this.notifications.data = [data.notification].concat(this.notifications.data);
      this.notificationService.read([data.notification]);
    }
  }

  private indexBehavior(data: Notifications) {
    if (data) {
      let lastData = this.notifications == null ? [] : this.notifications.data;
      let newData = data.data;
      this.notifications = Object.assign({}, data);
      this.notifications.data = lastData.concat(newData);
      let readList: Notification[] = [];
      for (var index = 0; index < data.data.length; index++) {
        if (!data.data[index].readed) {
          readList.push(data.data[index]);
        }
      }
      if (readList.length > 0) {
        this.notificationService.read(readList);
      }
    }
  }

}
