import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/do';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url.indexOf('api/guest') < 0) {
            const authRequest = request.clone({
                headers: request.headers.set('Authorization', localStorage.getItem('api_token'))
            });
            return next.handle(authRequest).do(
                success => { },
                error => {
                    if (error.status === 401) {
                        alert('You are not athenticated.');
                        this.router.navigate(['login']);
                    }
                }
            );
        } else {
            return next.handle(request);
        }
    }
}
