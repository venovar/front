import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionStoreComponent } from './store.component';

describe('TransactionStoreComponent', () => {
  let component: TransactionStoreComponent;
  let fixture: ComponentFixture<TransactionStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
