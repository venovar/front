import { NotificationService } from './../../../../service/auth/notification.service';
import { AccountService } from './../../../../service/auth/account.service';
import { TransactionService } from './../../../../service/auth/transaction.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User, Users } from '../../../../model/user';
import { FormArray } from '@angular/forms/src/model';
import { NotificationSocket } from '../../../../model/notification';

@Component({
  selector: 'app-transaction-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class TransactionStoreComponent implements OnInit {

  private page: number;
  private usersSelected: User[];
  public account: User;
  public users: Users;
  public amount: number;
  public sending: boolean;

  constructor(private transactionService: TransactionService, //
    private accountService: AccountService, //
    private notificationService: NotificationService
  ) {
    this.notificationService.notificationBehavior.subscribe(this.notificationBehavior.bind(this));
  }

  public ngOnInit() {
    this.amount = null;
    this.users = null;
    this.page = 1;
    this.usersSelected = [];
    this.account = this.accountService.getUser();
    this.index();
  }

  public index(term?: string) {
    this.sending = false;
    this.transactionService.users(this.page, term, this.usersResponse.bind(this));
    this.page++;
  }
  
  private notificationBehavior(data: NotificationSocket) {
    if (this.account && data && data.user) {
      this.account.amount = parseFloat(data.user.amount.toFixed(2));
    }
  }

  private usersResponse(data: Users) {
    if (this.users == null) {
      this.users = data;
    } else {
      let firstData = this.users.data;
      let newData = data.data;
      this.users = data;
      this.users.data = firstData.concat(newData);
    }
  }

  public usersChange(user: User, isChecked: boolean) {
    if (isChecked) {
      this.usersSelected.push(user);
    } else {
      this.usersSelected.splice(this.usersSelected.indexOf(user), 1);
    }
  }

  public send() {
    if(!this.sending) {
      this.sending = true;
      this.transactionService.store(this.amount, this.usersSelected, this.storeSuccess.bind(this), this.storeError.bind(this));
    }
  }

  private storeSuccess(messsage: string) {
    alert(messsage);
    this.ngOnInit();
  }

  private storeError(messsage: string) {
    this.sending = false;
    alert(messsage);
  }
}