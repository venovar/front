import { AccountService } from './../../../service/auth/account.service';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Transaction, Transactions } from './../../../model/transaction';
import { TransactionService } from './../../../service/auth/transaction.service';
import { User } from '../../../model/user';
import { NotificationService } from '../../../service/auth/notification.service';
import { NotificationSocket } from '../../../model/notification';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private page: number;
  public account: User;
  public transactions: Transactions;

  constructor(private notificationService: NotificationService, //
    private transactionService: TransactionService, //
    private accountService: AccountService
  ) {
    this.page = 1;
    this.notificationService.notificationBehavior.subscribe(this.notificationBehavior.bind(this));
  }

  public ngOnInit() {
    this.account = this.accountService.getUser();
    this.index();
  }

  public index() {
    this.transactionService.index(this.page, this.indexSuccess.bind(this));
    this.page++;
  }

  private notificationBehavior(data: NotificationSocket) {
    if (this.account && data && data.user) {
      this.account.amount = parseFloat(data.user.amount.toFixed(2));
    }
  }

  private indexSuccess(data: Transactions) {
    if (this.transactions == null) {
      this.transactions = data;
    } else {
      let firstData = this.transactions.data;
      let newData = data.data;
      this.transactions = data;
      this.transactions.data = firstData.concat(newData);
    }
  }
}
