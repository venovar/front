import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  RouterModule,
  Routes
} from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AuthComponent } from './auth.component';
import { User } from '../../model/user';
import { AuthGuard } from '../../guard/auth.guard';
import { AccountService } from '../../service/auth/account.service';
import { NotificationService } from '../../service/auth/notification.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotificationIndexComponent } from '../../page/auth/notification/index/index.component';
import { TransactionStoreComponent } from './transaction/store/store.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { TransactionService } from '../../service/auth/transaction.service';
import { AuthService } from '../../service/auth/auth.service';
import { AccountResolve } from './account.resolve';

const authRoutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    resolve: {
      account: AccountResolve
    },
    children: [
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        component: DashboardComponent,
      },
      {
        path: 'notification',
        children: [
          {
            path: 'index',
            canActivate: [AuthGuard],
            component: NotificationIndexComponent,
          }
        ]
      },
      {
        path: 'transaction',
        children: [
          {
            path: 'store',
            canActivate: [AuthGuard],
            component: TransactionStoreComponent,
          }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    NotificationIndexComponent,
    TransactionStoreComponent
  ],
  imports: [
    RouterModule.forChild(authRoutes),
    CommonModule,
    FormsModule,
    CurrencyMaskModule
  ],
  providers: [
    AccountResolve,
    AuthGuard,
    AuthService,
    AccountService,
    NotificationService,
    TransactionService
  ],
  exports: [
    RouterModule
  ]
})
export class AuthModule { }