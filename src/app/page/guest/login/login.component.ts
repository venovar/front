import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../service/auth/auth.service';
import { User } from '../../../model/user';
import {
  Router,
} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public account: User;
  public formMessage: string;

  constructor(private authService: AuthService, private router: Router) { }

  public ngOnInit() {
    this.account = <User>{};
  }

  public onSubmit() {
    this.formMessage = null;
    this.authService.login(this.account, this.loginSuccess.bind(this), this.loginError.bind(this));
  }

  private loginSuccess() {
    this.router.navigate(['dashboard']);
  }

  private loginError(message) {
    this.formMessage = "Invalid e-mail or password.";
  }
}