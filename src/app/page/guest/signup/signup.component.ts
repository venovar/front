import {
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { TransactionService } from '../../../service/auth/transaction.service';
import { UserService } from '../../../service/guest/user.service';
import { User } from '../../../model/user';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  public user: User;
  private userService: UserService;
  private router: Router;

  constructor(userService: UserService, router: Router) {
    this.user = <User>{};
    this.userService = userService;
    this.router = router;
  }

  public onSubmit() {
    this.userService.store(
      this.user,
      this.storeSuccess.bind(this),
      this.storeError
    );
  }

  private storeSuccess(message: string) {
    alert(message);
    this.router.navigate(['login']);
  }

  private storeError(message: string) {
    alert(message);
  }
}
