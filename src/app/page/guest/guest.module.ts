import { NgModule }               from '@angular/core';
import {
  RouterModule, 
  Routes, 
}                                 from '@angular/router';
import { FormsModule }            from '@angular/forms';

import { HttpClientModule }       from '@angular/common/http';

import { GuestComponent }         from './guest.component';
import { HomeComponent }          from './home/home.component';
import { LoginComponent }         from './login/login.component';
import { SignupComponent }        from './signup/signup.component';

import { TransactionService }     from '../../service/auth/transaction.service';
import { UserService }            from '../../service/guest/user.service';

const guestRoutes: Routes = [
  {
    path: '',
    component: GuestComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'signup',
        component: SignupComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    RouterModule.forChild(guestRoutes),
    FormsModule,
    HttpClientModule
  ],
  providers : [
    TransactionService,
    UserService
  ],
  exports: [
    RouterModule
  ]
})
export class GuestModule { }